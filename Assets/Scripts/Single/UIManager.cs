
using UnityEngine;

public class UIManager : ManagerClassBase<UIManager>
{
    public GameObject m_MapBackground;

    public bool isTowerBulid;

    SpriteRenderer m_SpriteRenderer;
    public void ChangeMapBackgroundColor()
    {
        SpriteRenderer spriteRenderer = m_MapBackground.GetComponent<SpriteRenderer>();
        m_SpriteRenderer = spriteRenderer;
        if (isTowerBulid)
            spriteRenderer.color = new Color(140f/255f, 140f/255f, 140f/255f);

        else
            spriteRenderer.color = Color.white;
    }
}