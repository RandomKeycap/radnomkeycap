using System.Collections;
using System.Collections.Generic;
using UnityEditor.Rendering;
using UnityEngine;

public class TowerObjectPool : MonoBehaviour
{
    private Tower _Tower;

    public Queue<TowerBullet> objectPool = new();

    [SerializeField]
    private TowerBullet poolObject;

    public Tower tower => _Tower ?? (_Tower = GetComponentInParent<Tower>());
    private void Awake()
    {
        InitializePool(20);
    }

    private void InitializePool(int initCount)
    {
        for (int i = 0; i < initCount; i++)
        {
            objectPool.Enqueue(CreatePoolingObject());
        }
    }

    private TowerBullet CreatePoolingObject()
    {
        TowerBullet newObject = Instantiate(poolObject,tower.transform.position,Quaternion.identity).GetComponent<TowerBullet>();
        newObject.towerObjectPool = this;
        newObject.gameObject.SetActive(false);
        newObject.transform.SetParent(transform);
        return newObject;
    }
    public TowerBullet GetBullet(EnemyCharacter target)
    {
        if (objectPool.Count > 0)
        {
            var obj = objectPool.Dequeue();
            obj.SetTarget(target);
            obj.gameObject.SetActive(true);
            obj.transform.SetParent(tower.transform);
            obj._Damage = tower.towerData._TowerAtk;
            return obj;
        }

        else
        {
            var newObj = CreatePoolingObject();
            newObj.SetTarget(target);
            newObj.gameObject.SetActive(true);
            newObj.transform.SetParent(tower.transform);
            return newObj;
        }
    }

    public void ReturnBullet(TowerBullet bullet)
    {
        bullet.gameObject.SetActive(false);
        bullet.transform.SetParent(transform);
        bullet.transform.position = tower.transform.position;
        objectPool.Enqueue(bullet);
    }
}
