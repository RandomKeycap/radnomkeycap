using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Tower : MonoBehaviour
{
    protected static TowerDataScriptableObject _TowerDataScriptableObject;

    public string m_ObjectCode;

    protected EnemyCharacter _Target;

    protected TowerObjectPool _TowerObjectPool;
    protected bool _isAttacking;
    public TowerObjectPool objectPool =>
        _TowerObjectPool ?? (_TowerObjectPool = GetComponentInChildren<TowerObjectPool>());

    public TowerData towerData { get; private set; }

    public char _KeyCap;

    public TMP_Text _KeyCapText;

    protected virtual void Awake()
    {
        if(_TowerDataScriptableObject == null)
            _TowerDataScriptableObject = Resources.Load<TowerDataScriptableObject>("ScriptableObject/TowerDataList");

        towerData = _TowerDataScriptableObject.Get(m_ObjectCode);
    }

    protected virtual void Update()
    {
        if (!_isAttacking)
        {
            StartCoroutine(AttackToTarget());
        }
    }


    public void SetKeycap(char keyCap)
    {
        _KeyCap = keyCap;
        _KeyCapText.text = _KeyCap.ToString();
    }
    protected virtual IEnumerator AttackToTarget()
    {
        _isAttacking = true; // 코루틴 실행 중임을 표시

        if (RoundManager.instance.enemyCharacterList.IsEmpty())
        {
            _isAttacking = false;
            yield break;
        }

        _Target = RoundManager.instance.enemyCharacterList[0];
        objectPool.GetBullet(_Target);

        // 3초 동안 대기
        yield return new WaitForSeconds(3f);

        _isAttacking = false; // 코루틴 실행 완료 표시
    }
}
