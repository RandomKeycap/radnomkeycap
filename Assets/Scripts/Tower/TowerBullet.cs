using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerBullet : MonoBehaviour
{
    public float m_BulletSpeed = 2.0f;

    private EnemyCharacter _Target;

    public TowerObjectPool towerObjectPool;

    public float _Damage;
    public void SetTarget(EnemyCharacter target)
        => _Target = target;

    private void Update()
        => ShootToTarget();

    private void ShootToTarget()
    {
        if(ReachedTarget())
        {
            _Target.ApplyDamage(_Damage);
            _Target = null;
            towerObjectPool.ReturnBullet(this);
            return;
        }
        transform.position = Vector3.MoveTowards(transform.position, _Target.transform.position, m_BulletSpeed * Time.deltaTime);

    }

    private bool ReachedTarget()
    {
        float distanceToTarget = Vector3.Distance(transform.position,_Target.transform.position);

        return distanceToTarget < 0.1f;
    }
}
