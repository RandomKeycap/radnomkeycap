using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public EnemyCharacter[] m_EnemyUnits;

    public GameObject m_EnemySpawnPos;

    public GameObject m_EnemyWayPointTransform;

    [SerializeField]
    private int _CurrentMonsterCount;


    private int _MaxSpawnCount = 10;

    [SerializeField]
    private float _SpawnCoolTime;

    [SerializeField]
    private float _RoundCoolTime;

    private int _SpawnEnemyIndex;

    public bool _IsAllowSpawn;

    private void Start()
    {
        StartCoroutine(Spawner());
    }

    private IEnumerator Spawner()
    {
        while (true)
        {
            yield return new WaitUntil(() => _IsAllowSpawn == true);

            // 몬스터 생성
            EnemyCharacter enemyUnit =
                Instantiate(m_EnemyUnits[_SpawnEnemyIndex].gameObject, m_EnemySpawnPos.transform.position, Quaternion.identity)
                .GetComponent<EnemyCharacter>();

            // 몬스터의 목적지 설정
            enemyUnit.enemyMovement.m_WayPointTransform = m_EnemyWayPointTransform;

            _CurrentMonsterCount++;
            RoundManager.instance.monsterCount++;
            RoundManager.instance.enemyCharacterList.Add(enemyUnit);

            // 몬스터를 최대치만큼 생성시키면 생성 중단하고 다음 몬스터 생성 준비
            if (_CurrentMonsterCount == _MaxSpawnCount)
            {
                _CurrentMonsterCount = 0;
                _SpawnEnemyIndex++;
                _IsAllowSpawn = false;

                yield return new WaitForSeconds(_RoundCoolTime);
                _IsAllowSpawn = true;
            }

            else
            {

                // 쿨타임만큼 대기
                yield return new WaitForSecondsRealtime(_SpawnCoolTime);
            }
        }
    }



}
