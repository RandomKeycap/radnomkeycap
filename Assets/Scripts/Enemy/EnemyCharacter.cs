using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EnemyCharacter : MonoBehaviour
{
    protected static EnemyHUD m_EnemyHUDPrefab;

    protected static EnemyDataScriptableObject m_EnemyDataScritpableObject;

    public string m_EnemyCode;

    public float currentHp;

    public EnemyData enemyData { get; private set; }

    protected GameObject healthBarPrefab;

    protected EnemyMovement _EnemyMovement;

    public Vector3 healthBarOffset = new Vector3(0, 1, 0);
    public EnemyMovement enemyMovement =>
        _EnemyMovement ?? (_EnemyMovement = GetComponent<EnemyMovement>());
    protected EnemyHUD enemyHUD { get; private set; }

    private TMP_Text healthBarText;

    private void Awake()
    {
        if (m_EnemyDataScritpableObject == null)
            m_EnemyDataScritpableObject = Resources.Load<EnemyDataScriptableObject>("ScriptableObject/EnemyDataList");

        if (m_EnemyHUDPrefab == null)
            m_EnemyHUDPrefab = Resources.Load<EnemyHUD>("Prefabs/UI/HUD");
        
        enemyData = m_EnemyDataScritpableObject.Get(m_EnemyCode);

        currentHp = enemyData.maxHp;

        healthBarText = GetComponentInChildren<TMP_Text>();
        healthBarText.text = currentHp.ToString();
    }



    /// <summary>
    /// HUD 객체가 생성될 경우 호출됩니다.
    /// </summary>
    protected virtual void OnHUDCreated()
    {
        // Get SceneInstance
        GameSceneInstance sceneInstance =
            SceneManagerBase.instance.GetSceneInstance<GameSceneInstance>();

        // Get PlayerUI
        GameUI playerUI = sceneInstance.m_GameUI;

        // 적 HUD 생성
        enemyHUD = playerUI.CreateHUD(m_EnemyHUDPrefab);

        // HUD에 적 정보 설정
        enemyHUD.SetHp(currentHp);
    }
    protected void OnHUDTick()
    {

        // HUD 위치 설정
        Vector3 hudWorldPosition = transform.position;
        hudWorldPosition.x += healthBarOffset.x;
        hudWorldPosition.y += healthBarOffset.y;

        enemyHUD.SetWorldPosition(hudWorldPosition);

        // 현재 체력 갱신
        enemyHUD.SetHp(currentHp);
    }

    public void ApplyDamage(float damage)
    {
        
        currentHp -= damage;
        if (currentHp <= 0)
        {
            RoundManager.instance.enemyCharacterList.Remove(this);
            Destroy(gameObject);
        }
        healthBarText.text = currentHp.ToString();
    }

}
