using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUDBase : MonoBehaviour
{
    private Vector3 _WorldPosition;

    private RectTransform rectTransform => transform as RectTransform;

    public void OnHUDInitialized()
    {
        rectTransform.anchorMin = rectTransform.anchorMax = Vector2.zero;
        rectTransform.pivot = new Vector2(0.5f, 0.0f);
    }

    public void SetWorldPosition(Vector3 worldPosition)
    {
        _WorldPosition = worldPosition;

        GameSceneInstance sceneInstance = SceneManagerBase.instance.GetSceneInstance<GameSceneInstance>();

        Camera cameraComponent = Camera.main;

        Vector3 screenPosition = cameraComponent.WorldToViewportPoint(_WorldPosition);
        screenPosition *= new Vector2((float)Screen.width, (float)Screen.height);

        rectTransform.anchoredPosition = screenPosition;


    }
}
