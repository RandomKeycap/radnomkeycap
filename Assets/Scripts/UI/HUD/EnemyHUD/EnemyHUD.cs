using TMPro;
using UnityEngine;
public class EnemyHUD : HUDBase
{

    public TMP_Text m_HpText;


    public void SetHp(float hp)
    {
        m_HpText.text = hp.ToString();
    }


}
