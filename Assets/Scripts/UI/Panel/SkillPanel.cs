using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SkillPanel : MonoBehaviour
{
    public Button m_TowerBatch;

    public GameObject m_Tower;

    private char[] _KeyCapArray = new char[26];
    private void Awake()
    {
        m_TowerBatch.GetComponentInChildren<TMP_Text>().text = Constants.TOWER_COAST.ToString();
        InitializeCharArray();
    }

    public void TowerBatch()
    {
        GameSceneInstance sceneInstance = SceneManagerBase.instance.GetSceneInstance<GameSceneInstance>();
        PlayerCharacter playerCharacter = sceneInstance.playerController.controlledCharacter as PlayerCharacter;

        if (playerCharacter.coast < Constants.TOWER_COAST)
        {
            Debug.Log("자원이 부족합니다.");
            return;
        }
        else
        {
            playerCharacter.coast -= Constants.TOWER_COAST;
            //Tower tower = Instantiate(m_Tower).GetComponent<Tower>();
            //tower.SetKeycap(GetRandomKeyCap());
            UIManager.instance.isTowerBulid = true;
            UIManager.instance.ChangeMapBackgroundColor();
        }

    }

    private void InitializeCharArray()
    {
        for(int i=0;i<26;i++)
        {
            _KeyCapArray[i] = (char)('A' + i);
        }
    }

    private char GetRandomKeyCap()
    {
        int randomIndex = Random.Range(0, _KeyCapArray.Length);
        return _KeyCapArray[randomIndex];
    }
}
