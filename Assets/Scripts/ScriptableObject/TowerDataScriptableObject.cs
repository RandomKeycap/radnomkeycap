using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObject/TowerData")]
public class TowerDataScriptableObject : ScriptableObject
{
    public List<TowerDataElem> dataElemList;

    public TowerData Get(string objectCode)
        => dataElemList.Find(elem => objectCode == elem.objectCode)._TowerData;

}

[Serializable]
public class TowerDataElem
{
    public string objectCode;
    public TowerData _TowerData;
}

[Serializable]
public struct TowerData
{
    public float _TowerAtk;
    
    public TowerType _TowerType;

    public float _TowerRate;
}