using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "ScriptableObject/EnemyData")]
public class EnemyDataScriptableObject : ScriptableObject
{
    public List<EnemyDataElem> enemyDataElem;

    public EnemyData Get(string objectCode)
        => enemyDataElem.Find(elem => elem.objectCode == objectCode).enemyData;
}

[Serializable]
public class EnemyDataElem
{
    public string objectCode;
    public EnemyData enemyData;
}

[Serializable]
public struct EnemyData
{
    public float maxHp;

    public Sprite icon;
}